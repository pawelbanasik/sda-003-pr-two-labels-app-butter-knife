package com.example.rent.sda_003_pr_two_labels_app_butter_knife;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.button_one)
    protected Button buttonOne;

    @BindView(R.id.text_view_one)
    protected TextView textViewOne;

    @BindView(R.id.text_view_two)
    protected TextView textViewTwo;

    private int clickCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    // nie trzeba robic przypisania tej on click method w designerze
    // bo ta anotacja wywoluje automatycznie
    @OnClick(R.id.button_one)
    public void onClickMethod(View v) {
        if (clickCounter == 0) {
            textViewOne.setText("Kliknalem pierwszy raz");
        } else if (clickCounter == 1) {
            textViewTwo.setText("Kliknalem drugi raz");
        }
        clickCounter++;
    }


}

